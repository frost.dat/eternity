FROM python:3.6-alpine

ENV PYTHONUNBUFFERED 1

COPY . /app
WORKDIR /app

RUN apk update \
&& apk add postgresql-dev gcc python3-dev musl-dev \
&& python3 -m pip install --upgrade pip \
&& pip3 install --no-cache-dir -r requirements.txt

WORKDIR /app/eternity

# CMD [ "/bin/sh", "-c", "python3 manage.py runserver" ]

EXPOSE 8000
