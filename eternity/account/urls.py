from django.urls import path
from .views import *

urlpatterns = [
    path('users/<str:username>', AccountView.as_view()),
    # path('create', AccountCreate.as_view()),
]
